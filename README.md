# Scheduling of Elective Patients Considering Upstream and Downstream Units and Emergency Demand Using Robust Optimization #

This repository includes instances and solutions for both of the certainty and uncertainty conditions in the paper entitled "Scheduling of Elective Patients Considering Upstream and Downstream Units and Emergency Demand Using Robust Optimization".


For more clarification, you can download the paper mentioned above from the following link:
https://www.sciencedirect.com/science/article/pii/S0360835218301918

Note that all indices, scalars, and parameters are defined in a way that intersted readers can relate them to the ones provided in the paper.

# General Specifications #

https://bitbucket.org/Pro_Data/instances/downloads/General%20Specifications.docx

# Certainty Condition #
- Specification:

1- https://bitbucket.org/Pro_Data/instances/downloads/1%20-%2020%20-%20D.txt

2- https://bitbucket.org/Pro_Data/instances/downloads/2%20-%2025%20-%20D.txt

3- https://bitbucket.org/Pro_Data/instances/downloads/3%20-%2030%20-%20D.txt

4- https://bitbucket.org/Pro_Data/instances/downloads/4%20-%2035%20-%20D.txt

5- https://bitbucket.org/Pro_Data/instances/downloads/5%20-%2040%20-%20D.txt

6- https://bitbucket.org/Pro_Data/instances/downloads/6%20-%2045%20-%20D.txt

7- https://bitbucket.org/Pro_Data/instances/downloads/7%20-%2050%20-%20D.txt

8- https://bitbucket.org/Pro_Data/instances/downloads/8%20-%2055%20-%20D.txt

9- https://bitbucket.org/Pro_Data/instances/downloads/9%20-%2060%20-%20D.txt

10- https://bitbucket.org/Pro_Data/instances/downloads/10%20-%2070%20-%20D.txt


- Solution:

Cplex:
https://bitbucket.org/Pro_Data/instances/downloads/Certainty%20-%20CPLEX.txt

MIP-based LNS:
https://bitbucket.org/Pro_Data/instances/downloads/Certainty%20-%20MIP-based%20LNS.txt

# Unertainty Condition #
- Specification:

11- https://bitbucket.org/Pro_Data/instances/downloads/11%20-%2020%20-%20R.txt

12- https://bitbucket.org/Pro_Data/instances/downloads/12%20-%2025%20-%20R.txt

13- https://bitbucket.org/Pro_Data/instances/downloads/13%20-%2030%20-%20R.txt

14- https://bitbucket.org/Pro_Data/instances/downloads/14%20-%2035%20-%20R.txt

15- https://bitbucket.org/Pro_Data/instances/downloads/15%20-%2040%20-%20R.txt

16- https://bitbucket.org/Pro_Data/instances/downloads/16%20-%2045%20-%20R.txt

17- https://bitbucket.org/Pro_Data/instances/downloads/17%20-%2050%20-%20R.txt

18- https://bitbucket.org/Pro_Data/instances/downloads/18%20-%2055%20-%20R.txt

19- https://bitbucket.org/Pro_Data/instances/downloads/19%20-%2060%20-%20R.txt

20- https://bitbucket.org/Pro_Data/instances/downloads/20%20-%2070%20-%20R.txt


- Solution:

Cplex:
https://bitbucket.org/Pro_Data/instances/downloads/Uncertainty%20-%20CPLEX.txt

MIP-based LNS:
https://bitbucket.org/Pro_Data/instances/downloads/Uncertainty%20-%20MIP-based%20LNS.txt